# core modules
import os
import time
import re

# advanced modules
import logging
import logging.handlers

# custom modules
from utility.exceptions import *
import utility.textmode
from utility.caching import lru_cache

# dummy reassignment to allow importing just report
class LVL(object):
	CRITICAL = logging.CRITICAL
	ERROR    = logging.ERROR
	WARNING  = logging.WARNING
	STATUS   = (logging.WARNING + logging.INFO) / 2
	INFO     = logging.INFO
	DEBUG    = logging.DEBUG
	NOTSET   = logging.NOTSET

_stdFrmt = '[%(asctime)s] %(message)s'
_vvvFrmt = "[%(asctime)s] %(name)-20s [ %(levelname)-10s ] >> %(funcName)20s at %(filename)s:%(lineno)03d [ Process:%(process)s Thread:%(thread)s ]\n\t%(message)s"


def log(scope, level, msg, *args, **kwArgs):
	logging.getLogger(scope).log(level, msg, *args, exc_info = kwArgs.get('exc_info'), extra = kwArgs.get('extra', {}))


class LogRotateHandler(logging.handlers.BaseRotatingHandler):
	"""
	Handler for logging to a sequence of files, rotating when either a time or
	sizelimit is reached.

	This handler works similar to the logrotate utility. When writing, the age
	and size of the log file is checked. If either exceeds the limit, a new log
	file is created. A number of old files can be kept, which are deleted
	sequentially when new logs are added.

	:param filename: the (pseudo-) file to which messages are logged.
	:type filename: str, unicde
	:param maxBytes: maximum log file size in bytes
	:type maxBytes: int, float
	:param maxAge: maximum log file age in seconds
	:type maxAge: int, float
	:param maxCount: maximum number of log files to keep
	:type maxCount: int
	:param encoding: encoding to use for writing
	:type encoding: str, unicode, None
	:param delay: delay opening files until first write
	:type delay: bool
	"""
	def __init__(self, filename, maxBytes = -1, maxAge = -1, maxCount = 3, encoding = None, delay = False):
		logging.handlers.BaseRotatingHandler.__init__(
			self,
			filename=filename,
			mode='a',
			encoding=encoding,
			delay=False
			)
		self.maxBytes  = maxBytes
		self.maxAge    = maxAge
		self.maxCount  = maxCount
		self.timestamp = time.time()
		if os.path.exists(filename):
			self.timestamp = os.stat(filename).st_mtime

	def shouldRollover(self, record):
		"""Test if writing would require rotating the log file first"""
		if self.stream is None:                      # is delay in effect?
			self.stream = self._open()
		if self.maxBytes > 0:                        # is size limited?
			msg = "%s\n" % self.format(record)
			self.stream.seek(0,2)                    # from RotatingHandler
			if self.stream.tell() + len(msg) > self.maxBytes:
				return True
		if self.maxAge > 0:                          # is age limited?
			if time.time() - self.timestamp > self.maxAge:
				return True
		return 0

	def doRollover(self):
		"""Rotate the collection of log files and open a new one"""
		self.timestamp = time.time()
		if self.stream:                              # disable active stream
			self.stream.close()
			self.stream = None
		if self.maxCount > 0:                        # backups allowed, rotate
			for ind in xrange(self.maxCount, 1, -1): # rotate backups
				newLogName = '%s.%s' % (self.baseFilename, ind)
				oldLogName = '%s.%s' % (self.baseFilename, ind - 1)
				if os.path.exists(oldLogName):       # avoid collision
					if os.path.isfile(newLogName) or os.path.islink(newLogName):
						os.unlink(newLogName)
					os.rename(oldLogName, newLogName)
			newLogName = self.baseFilename + '.1'    # rotate current
			oldLogName = self.baseFilename
			if os.path.exists(oldLogName):
					if os.path.isfile(newLogName) or os.path.islink(newLogName):
						os.unlink(newLogName)
					os.rename(oldLogName, newLogName)
		else:                                        # backups disabled, clean up
			if os.path.isfile(self.baseFilename) or os.path.islink(self.baseFilename):
				os.unlink(self.baseFilename)
		if not self.delay:
			self.stream = self._open()


# Formatting
# ------------------------------------------------------------------------------


class RecordFormatter(logging.Formatter):
	"""
	Formatter capable of applying advanced formats depending on record features

	:type record_fmt_mapper: Function that maps record attributes to format strings
	"""
	def __init__(self, fmt=None, datefmt=None, record_fmt_mapper = None):
		logging.Formatter.__init__(self, fmt = fmt, datefmt = datefmt)
		self.record_fmt_mapper = record_fmt_mapper or self.record_fmt_mapper

	def format(self, record):
		# See logging.Formatter
		record.message = record.getMessage()
		record.asctime = self.formatTime(record, self.datefmt)
		fmt =  self.record_fmt_mapper(record) or self._fmt
		s = fmt % record.__dict__
		if record.exc_info:
			if not record.exc_text:
				record.exc_text = self.formatException(record.exc_info)
		if record.exc_text:
			if s[-1:] != "\n":
				s += "\n"
			try:
				s += record.exc_text
			except UnicodeError:
				s += record.exc_text.decode(sys.getfilesystemencoding(), 'replace')
		return s

	def record_fmt_mapper(self, record):
		section = loggerSectionFmt(record.name)
		if record.levelno >= LVL.CRITICAL:
			return '[%(asctime)s] /!\ ' + section + ': %(message)s'
		if record.levelno >= LVL.ERROR:
			return '[%(asctime)s] <!> ' + section + ': %(message)s'
		if record.levelno >= LVL.WARNING:
			return '[%(asctime)s]  !  ' + section + ': %(message)s'
		if record.levelno >= LVL.STATUS:
			return '[%(asctime)s]  =  ' + section + ': %(message)s'
		if record.levelno >= LVL.INFO:
			return '[%(asctime)s]  ?  ' + section + ': %(message)s'
		return     '[%(asctime)s]  .  ' + section + ': %(message)s'


class TerminalFormatter(RecordFormatter):
	"""
	Formatter for creating output for terminals

	This Formatter will automatically limit the width of statements to a fixed
	length. In addition, automatic colorization depending on the severity level
	can be applied.

	:param colorize: automatically colorize messages according to their severity
	:type colorize: bool
	:param max_width: limit message length
	:type max_width: int
	"""
	_ansiRE = re.compile("\003\[\d+m")
	def __init__(self, fmt=None, datefmt=None, record_fmt_mapper = None, colorize = True, max_width = -1):
		RecordFormatter.__init__(
			self,
			fmt = fmt, datefmt = datefmt,
			record_fmt_mapper = record_fmt_mapper
			)
		self._widthlimit = max_width
		self.colorize = colorize

	def format(self, record):
		def resize(out):
			if self._widthlimit > 0:
				ansilen  = out.count('\x1b')
				printlen = len(out) - ansilen
				if printlen > self._widthlimit:
					return out[:(self._widthlimit+ansilen*3-3)]+'...'
			return out
		s = RecordFormatter.format(self, record)
		s.replace('\t','  ')
		if self.colorize:
			return self.colorizer(record) + resize(s) + utility.textmode.MODE.RESET
		return resize(s)

	def colorizer(self, record):
		if record.name.upper().startswith('EXCEPT'):
			return ''
		if record.levelno >= LVL.CRITICAL:
			return utility.textmode.MODE.BOLD + utility.textmode.MODE.ITALIC + utility.textmode.TONE.RED
		if record.levelno >= LVL.ERROR:
			return utility.textmode.MODE.ITALIC + utility.textmode.TONE.RED
		if record.levelno >= LVL.WARNING:
			return utility.textmode.MODE.BOLD + utility.textmode.TONE.ORANGE
		if record.levelno >= LVL.STATUS:
			return utility.textmode.MODE.BOLD + utility.textmode.TONE.GREEN
		if record.levelno >= LVL.INFO:
			return utility.textmode.TONE.BLUE
		return ''


class LogFileFormatter(RecordFormatter):
	"""
	Formatter for creating log file output

	This formatter filters ANSI escape sequences from messages, creating plain ASCII log files.

	:param strip_ansi: remove all ANSI text modifiers from messages
	:param strip_ansi: bool
	"""
	_ansiRE = re.compile("\x1b\[\d+m")
	def __init__(self, fmt=None, datefmt=None, record_fmt_mapper = None, strip_ansi = True):
		RecordFormatter.__init__(
			self,
			fmt = fmt, datefmt = datefmt,
			record_fmt_mapper = record_fmt_mapper
			)
		self._stripAnsi = strip_ansi

	def format(self, record):
		s = RecordFormatter.format(self, record)
		if self._stripAnsi:
			return self._ansiRE.sub('', s)
		return s


@lru_cache(16)
def loggerSectionFmt(name = "NOTSET"):
	"""Helper for neatly showing logger sections"""
	name = name.partition('.')[0].lower()
	name = name[0].upper() + name[1:]
	return name.ljust(10, '_')


# Configuration/Setup
# ------------------------------------------------------------------------------

_date_fmt = '%y%m%d-%H:%M:%S'
_dflt_fmt = "[%(asctime)s] %(levelno)d:%(name)-20s >> %(message)s"


def update_parser(argumentParser):
	"""Update an argparse ArgumentParser with module specific entries"""
	report_parser = argumentParser.add_argument_group(title="report arguments")
	report_parser.add_argument(
		'--log-output',
		nargs='*',
		default=['stream:stderr'],
		help='Destination for initialization reports',
		)
	report_parser.add_argument(
		'--log-level',
		default='CRITICAL',
		choices=['DISABLED','CRITICAL', 'ERROR', 'WARNING', 'STATUS', 'INFO', 'DEBUG'],
		help='Loglevel for initialization reports',
		)


def argparse_init(argparseNamespace):
	"""Apply a basic configuration from argparse for initialization"""
	initHandlers = []
	for output in argparseNamespace.log_output:
		if output.startswith('stream:'):
			stream = output.split(':')[1]
			if stream not in ['stdout', 'stderr']:
				raise ValueError("Expected 'stream:stderr' or 'stream:stdout'")
			handler = logging.StreamHandler(getattr(sys, stream))
			handler.setFormatter(
					TerminalFormatter(
						fmt=_dflt_fmt,
						datefmt=_date_fmt,
						max_width=-1,
						)
				)
			initHandlers.append(handler)
		elif output.startswith('file:'):
			filename = output.split(':')[1]
			handler = LogRotateHandler(filename=filename, maxBytes=50*1000, maxCount=5)
			handler.setFormatter(
				logging.Formatter(
					fmt=_dflt_fmt,
					datefmt=_date_fmt,
					)
			)
			initHandlers.append(handler)
		else:
			raise ValueError("Expected 'stream:stderr', 'stream:stdout' or 'file:<filename>]'")
	logging.getLogger().handlers = initHandlers
	logging.getLogger().setLevel(getattr(LVL, argparseNamespace.log_level))


def configure_module(config = {}):
	"""
	Perform a basic logging initialization
	:param config:
	:return:
	"""
	logging.getLogger().handlers = []
	handler = logging.StreamHandler()
	handler.setFormatter(
		logging.Formatter(
			fmt = _stdFrmt,
			datefmt = '%y%m%d-%H:%M:%S'
			)
		)
	if 'diagnose' in config:
		handler.setFormatter(
			logging.Formatter(
				fmt = _vvvFrmt,
				datefmt = '%y%m%d-%H:%M:%S'
				)
			)
	logging.getLogger().addHandler(handler)
	logging.getLogger().setLevel(logging.NOTSET)
configure_module()

