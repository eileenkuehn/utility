"""
Provides custom Exceptions and Errors and some convenience tools to handle them.
"""

# core modules
import sys
import logging
import linecache

# advanced modules

# custom modules
from . import textmode


class Sentinel(object):
	"""Placeholder for default values"""
	def __init__(self, name):
		self.name = name
	def __str__(self):
		return self.name
	def __repr__(self):
		return '<%s>' % self.name


def _get_exception_cause(exception_instance):
	"""Extract the cause of an exception from the most appropriate source"""
	if not hasattr(exception_instance, '__dict__'):
		return
	if exception_instance.__dict__.get("__cause__") is not None:
		return exception_instance.__dict__.get("__cause__")
	elif exception_instance.__dict__.get("__context__") is not None and not exception_instance.__dict__.get("__suppress_context__"):
		return exception_instance.__dict__.get("__context__")
	return None


def _get_exception_chain(exception_instance):
	"""Extract the chain of exception instances causing each other"""
	parent_exception = _get_exception_cause(exception_instance)
	if parent_exception is None:
		return [exception_instance]
	else:
		return [exception_instance] + _get_exception_chain(parent_exception)


no_cause = Sentinel('No Cause')


def exc_from(new, cause=no_cause):
	"""
	Create a chain of exceptions

	:param new: new exception to raise
	:param cause: old exception that directly led to new exception
	:param context: old exception that was handled when the new one occured

	:note: This is a compatibility function for python2.X for the python3.X
		   functionality of chaining exceptions. The 3.X statement
		   `raise new from cause` is equivalent to `raise exc_from(new, cause)`.
		   If cause is explicitly set `None`, a chain is created but suppressed.
		   In 3.X, an exception context is set automatically; this function will
		   set it as well. Using `raise exc_from(new)` is sufficient.

	:note: Python2.X does not add the traceback to exceptions. This function
		   adds the traceback to the exception *context*. If cause and context
		   are the same exceptions, the later implicitly has the traceback made
		   available as well.
	"""
	current_exc_type, current_exc_instance, current_exc_traceback = sys.exc_info()
	current_exc_instance.__traceback__ = current_exc_traceback
	del current_exc_traceback
	# catch 'raise ValueError' vs 'raise ValueError()'
	if type(new) == type:
		new = new()
	if cause is None:
		new.__suppress_context__ = True
	new.__cause__ = cause if cause is not no_cause else None
	new.__context__ = current_exc_instance
	return new


def logTraceback(exClass, exValue, traceback, logger='EXCEPTION', add_chain_header=True):
	"""
	Write traceback to logger

	:param exClass: type of exception raised
	:param exValue: instance of exception raised
	:param traceback:
	:param logger: target for logging
	:param add_chain_header: add a header if this exception if part of a chain
	"""
	logger = logging.getLogger(logger)
	# late-bind to allow disabling
	TONE, MODE = textmode.TONE, textmode.MODE
	# handle chained tracebacks
	parent_exception = _get_exception_cause(exValue)
	if parent_exception is not None:
		# add chain header for first element only
		if add_chain_header:
			parent_chain = _get_exception_chain(exValue)
			logger.critical("    .")
			if exClass is not None:
				exception_message = formatException(exClass, exValue, traceback)
				for line in exception_message.strip().splitlines():
					logger.critical('>>>>| %s%s%s%s', MODE.BOLD, TONE.RED, line, MODE.RESET)
				logger.warning('>>>>| %s%s[Exception Chain: %s]%s', MODE.BOLD, TONE.ORANGE, "->".join(exc.__class__.__name__ for exc in parent_chain), MODE.RESET)
				logger.critical('    | =====================================')
		# log previous exceptions in the chain
		logTraceback(
			exClass=type(parent_exception),
			exValue=parent_exception,
			traceback=parent_exception.__dict__.get('__traceback__', None),
			logger=logger.name,
			add_chain_header=False)
		logger.critical("    .")
		# start logging current exception
		if exValue.__dict__.get("__cause__"):
			logger.critical('    | %s%sThe above exception was the direct cause of the following exception:%s', MODE.BOLD, TONE.ORANGE, MODE.RESET)
		else:
			logger.critical('    | %s%sWhile handling the above exception, the following exception occured:%s', MODE.BOLD, TONE.ORANGE, MODE.RESET)
		logger.critical("    .")
	# log traceback
	tracebackDepth = 0
	logger.critical("    .")
	if exClass is not None:
		exMessage = formatException(exClass, exValue, traceback)
		for line in exMessage.strip().splitlines():
			logger.critical('>>>>| %s%s%s%s', MODE.BOLD, TONE.RED, line, MODE.RESET)
		logger.critical('    | =====================================')
	while traceback:
		logger.critical("    .")
		tracebackDepth +=1
		exCode = traceback.tb_frame.f_code
		linecache.checkcache(exCode.co_filename)
		# Exception position
		logger.critical(  '____|%s%s%s Traceback #%02d %-20s [%s:%03d]%s', MODE.UNDERLINE, MODE.BOLD, TONE.BLUE, tracebackDepth, "'%s'"%exCode.co_name, exCode.co_filename, traceback.tb_lineno, MODE.RESET)
		fmtLine = lambda line_no: linecache.getline(exCode.co_filename, line_no).rstrip().replace('\t', '  ')
		logger.debug(    '%03d | %s', (traceback.tb_lineno - 5), fmtLine(traceback.tb_lineno - 5))
		logger.debug(    '%03d | %s', (traceback.tb_lineno - 4), fmtLine(traceback.tb_lineno - 4))
		logger.info(     '%03d | %s', (traceback.tb_lineno - 3), fmtLine(traceback.tb_lineno - 3))
		logger.info(     '%03d | %s', (traceback.tb_lineno - 2), fmtLine(traceback.tb_lineno - 2))
		logger.error(  '%s%s%03d | %s%s', MODE.BOLD, TONE.ORANGE, (traceback.tb_lineno - 1), fmtLine(traceback.tb_lineno - 1), MODE.RESET)
		logger.critical('%s%s  =>|>%s%s', MODE.BOLD, TONE.RED, fmtLine(traceback.tb_lineno + 0), MODE.RESET)
		logger.error(  '%s%s%03d | %s%s', MODE.BOLD, TONE.ORANGE, (traceback.tb_lineno + 1), fmtLine(traceback.tb_lineno + 1), MODE.RESET)
		logger.info(     '%03d | %s', (traceback.tb_lineno + 2),fmtLine(traceback.tb_lineno + 2))
		logger.info(     '%03d | %s', (traceback.tb_lineno + 3),fmtLine(traceback.tb_lineno + 3))
		logger.debug(    '%03d | %s', (traceback.tb_lineno + 4),fmtLine(traceback.tb_lineno + 4))
		logger.debug(    '%03d | %s', (traceback.tb_lineno + 5),fmtLine(traceback.tb_lineno + 5))
		logger.critical(  '----+ ----------')
		# Output local and class variables
		def formatRepr(obj):
			try:
				objRepr = repr(obj)
				if len(objRepr) > 500:
					return objRepr[:497] + '...'
				return objRepr
			except Exception:
				return '<not representable>'
		def logVardict( varDict, prefix = '', log = logger.warning):
			maxlen = max(map(len, varDict.keys()) + [0])
			for varName in sorted(varDict.keys()):
				log('    |  %s%s = %s', prefix, varName.ljust(maxlen), formatRepr(varDict[varName]))
		localVars = dict(traceback.tb_frame.f_locals)
		localCls  = localVars.pop('self', None)
		logger.warning(    '____|%s%s Local variables     %s', MODE.UNDERLINE, MODE.BOLD, MODE.RESET)
		logVardict(localVars)
		logger.warning(    '----+ ----------')
		if localCls is not None:
			try:
				logger.warning(    '____|%s%s Class variables     (%s)%s', MODE.UNDERLINE, MODE.BOLD, formatRepr(localCls), MODE.RESET)
				classVars = {}
				if getattr(localCls, '__dict__', []): # default classes
					classVars = getattr(localCls,'__dict__', {})
				elif hasattr(localCls, '__slots__'): # get_signature'd classes
					logVardict({'__slots__' : getattr(localCls,'__slots__')})
					classVars = dict((name, getattr(localCls, name)) for name in getattr(localCls,'__slots__'))
				elif hasattr(localCls,'_fields '): # namedtuple
					logVardict({'_fields' : getattr(localCls,'_fields')})
					classVars = dict((name, getattr(localCls, name)) for name in getattr(localCls,'_fields'))
				logVardict(classVars)
				logger.warning(    '----+ ----------')
			except:
				logger.warning(    '    | <not representable>')
				logger.warning(    '----+ ----------')
		logger.critical("    '")
		traceback = traceback.tb_next
	if exClass is not None:
		exMessage = formatException(exClass, exValue, traceback)
		logger.critical("    .")
		logger.critical('    | =====================================')
		for line in exMessage.strip().splitlines():
			logger.critical('>>>>| %s', line)
	del traceback


def formatException(exClass = None, exValue = None, traceback = None):
	"""Format an Exception to a short description"""
	del traceback
	if exClass is not None:
		if exValue is not None:
			try:
				cls_len = len(exClass.__name__) + 2
				messages = exClass.message.splitlines()
				descr = '%s: %s' % (exClass.__name__, next(messages, ''))
				for message in messages:
					descr += ' ' * cls_len + message
				return descr
			except AttributeError:
				return '%s: %s\n' % (exClass.__name__, str(exValue))
		return '%s\n' %  exClass.__name__


def logException(logger = 'EXCEPTION'):
	"""Log a full description of the current exception"""
	return logTraceback(*sys.exc_info(), logger = logger)


def mainExceptionFrame(mainFunction, *mainArgs, **mainKWArgs):
	"""Execute a function with full error tracing"""
	try:
		mainFunction(*mainArgs, **mainKWArgs)
	except SystemExit: # Forward SystemExit exit code
		sys.exit(sys.exc_info()[1].code)
	except:
		logException()
		sys.stderr.write(formatException(*sys.exc_info()))
		sys.exit(1)


class ExceptionFrame(object):
	"""
	Context with a full stack trace if an error occurs

	:param onExceptTerminate: allow termination of the runtime
	:type onExceptTerminate: bool
	:param logger: name of the logger handling the stack trace
	:type logger: str
	:param ignore: exceptions to ignore and pass to outer scope
	:type ignore: list[Exception] or None
	"""
	def __init__(self, onExceptTerminate = True, logger = 'EXCEPTION', ignore = None):
		self._logger = logger
		self._exit   = onExceptTerminate
		self._ignore = ignore or []
	def __enter__(self):
		return self
	def __exit__(self, eType, eValue, eTrace):
		if eType is None:                      # Nothing to see here, move along
			return True
		if eType in self._ignore:
			return False
		if self._exit and eType == SystemExit: # Forward SystemExit exit code
			sys.exit(getattr(eValue,"code",1))
		if eType == KeyboardInterrupt:         # Manual break, no error
			exMessage = formatException(eType, eValue, eTrace)
			logger = logging.getLogger(self._logger)
			logger.critical("")
			logger.critical("    .")
			logger.critical('    | =====================================')
			for line in exMessage.strip().splitlines():
				logger.critical('>>>>| %s', line)
			sys.exit(1)
		logTraceback(eType, eValue, eTrace, logger = self._logger)  # no special case, full log
		if self._exit:
			sys.exit(1)


# Basic exceptions/errors
class BasicException(Exception):
	"""Base class for all custom exceptions"""
	def __init__(self, *args, **kwargs):
		Exception.__init__(self, *args, **kwargs)
	def __str__(self):
		"""Explicit display as exception for printing"""
		return "%s: %s" % (self.__class__.__name__, Exception.__str__(self))


class BasicError(BasicException):
	"""Base class for all custom errors"""
	pass


class RethrowException(BasicException):
	"""Add additional detail to a thrown exception"""
	def __init__(self, msg, exClass = BasicException):
		prevInfo = formatException(*sys.exc_info())
		if isinstance(sys.exc_info()[1], KeyboardInterrupt):
			BasicException.__init__(self, 'KeyboardInterrupt')
		else:
			raise exClass('%s\n%s'%(msg,prevInfo))


class TodoError(BasicException):
	"""Something needs to be done here"""
	def __init__(self, description = "You are not done here! At least add a proper TODO note next time!"):
		BasicException.__init__(self, 'TODO: %s' % description)


# Core exceptions/errors
class FileNotFound(BasicError):
	"""The requested file does not exist"""
	pass


class PermissionDenied(BasicError):
	"""The requested operation is not permitted"""
	pass


class InstallationError(BasicError):
	"""Local installation is insufficient"""
	pass


class APIError(BasicError):
	"""API misuse"""
	pass


class AbstractError(APIError):
	"""Abstract implementation call"""
	def __init__(self):
		APIError.__init__(self, "%s is an abstract implementation!" % sys._getframe(1).f_code.co_name)


class DeprecatedError(APIError):
	"""Deprecated implementation call"""
	def __init__(self):
		APIError.__init__(self, "%s is a deprecated implementation!" % sys._getframe(1).f_code.co_name)


class CommunicationError(BasicException):
	"""Communication with (external) resource failed"""
	def __init__(self, resource = "<unknown>"):
		BasicException.__init__(self, "Failed communication with %s" % resource)


class ValidationError(BasicError):
	"""An operation failed due to mismatch in validation credentials"""
	pass
