#!/usr/bin/env python

import setuptools
from distutils.core import setup

setup(
    name="utility",
    version="0.1",
    description="Utility functionality",
    author="Max Fischer",
    author_email="max.fischer@kit.edu",
    url="https://bitbucket.org/eileenkuehn/utility",
    packages=setuptools.find_packages(),
)
